clear all;
close all;
clc;
im = imread('dress.png');
hsb = rgb2hsv(im)*255;

skinMask = hsb(:,:,1) > 0 & hsb(:,:,1) < 20;
skinMask = skinMask & (hsb(:,:,2) > 48 & hsb(:,:,2) < 255);
skinMask = skinMask & (hsb(:,:,3) > 80 & hsb(:,:,3) < 255);
skinMask = imclose(skinMask,strel('disk',6));

%stage 2: calculate top, left and right centroid from the different connected
%components of the skin
stats = regionprops(skinMask,'centroid');
topCentroid = stats(1).Centroid;
rightCentroid = stats(1).Centroid;
leftCentroid = stats(1).Centroid;
for x = 1 : length(stats)
    centroid = stats(x).Centroid;
    if topCentroid(2)>centroid(2)
        topCentroid = centroid;
    elseif centroid(1)<leftCentroid(1)
        leftCentroid = centroid;
    elseif centroid(1)>rightCentroid(1)
        rightCentroid = centroid;
    end
end

%first seed - the average of the most left and right centroids.
centralSeed = int16((rightCentroid+leftCentroid)/2);

%second seed - a pixel which is right below the face centroid.
faceSeed = int16(topCentroid);
faceSeed(2) = faceSeed(2)+40; 

%stage 3: std filter
varIm = stdfilt(rgb2gray(im));

%stage 4 - region growing on varIm  using faceSeed and centralSeed
res1=regiongrowing(varIm,centralSeed(2),centralSeed(1),8);
res2=regiongrowing(varIm,faceSeed(2),faceSeed(1),8);
res = res1|res2;

%noise reduction
res = imclose(res,strel('disk',3));
res = imopen(res,strel('disk',2));
figure, imshow(res);
imwrite(res,'dress-bw.png');
[h, w] = size(res);
for i = 1:h
    for j=1:w
        if res(i, j) == 0
            im(i, j, :) = 255;
        end
    end
end
figure, imshow(im);
imwrite(im,'dress-seg.png');
